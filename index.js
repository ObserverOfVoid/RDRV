import {EL, addEL, CSV} from "./lib.js";

//Input

async function acceptData(name, csv) {
  let option = EL.input.type.querySelector(`[value=${name}]`);
  if (!option) {
    window.alert(`${name}: Unknown filename`);
    return;
  }
  let pf = proccessFile[name];
  if (!pf) {
    window.alert(`${name}: Not yet supported`);
    return;
  }
  if (option.disabled) {
    window.alert(`${name}: Already added`);
    return;
  }
  option.disabled = true;;
  let tab = await addTab(pf.tab), data = CSV.parse(csv, conversions);
  if (pf.run) pf.run(data, tab.module);
    else tab.module[`load${pf.load}`](data);
  let view = document.createElement("a");
  view.textContent = "View";
  view.onclick = e => {
    e.preventDefault();
    setTab(pf.tab);
  };
  view.href = "";
  for (let row of document.querySelectorAll("#filetable tbody tr")) if (row.children[0].textContent === `${name}.csv`) {
    row.replaceChild(view, row.children[2]);
    break;
  }
}

async function acceptFile(file) {
  if (file.type !== "text/csv") {
    window.alert(`${file.name}: Wrong filetype (${file.type})`);
    return;
  }
  await acceptData(file.name.replace(/\.csv$/, ""), await file.text());
}

async function acceptFiles(files) {
  for (let file of files) await acceptFile(file);
}

EL.input.file.onchange = () => acceptFiles(EL.input.file.files);

EL.input.area.ondragover = EL.input.area.ondragenter = e => e.preventDefault();

EL.input.area.ondrop = e => {
  e.preventDefault();
  acceptFiles(e.dataTransfer.files);
};

EL.input.submit.onclick = () => {
  if (EL.input.type.value === "") {
    window.alert("Select a file.");
    return;
  }
  acceptData(EL.input.type.value, EL.input.csv.value);
  EL.input.type.value = null;
  EL.input.csv.value = "";
};

//Data

const convs = {
  time: str => str ? ({str, time: Date.parse(str)}) : null
}, conversions = {
  created_at: convs.time,
  updated_at: convs.time
}, proccessFile = {
  account_gender: {
    tab: "misc",
    load: "Gender"
  },
  approved_submitter_subreddits: {
    tab: "subreddits",
    run(data, subreddits) {
      subreddits.add("approved", data);
    }
  },
  chat_history: {
    tab: "chat",
    load: "Data"
  },
  moderated_subreddits: {
    tab: "subreddits",
    run(data, subreddits) {
      subreddits.add("mod", data);
    }
  },
  statistics: {
    tab: "misc",
    load: "Statistics"
  },
  subscribed_subreddits: {
    tab: "subreddits",
    run(data, subreddits) {
      subreddits.add("member", data);
    }
  }
}, Data = {
  values: Object.create(null),
  listaners: Object.create(null),
  getValue(key, later) {
    if (Object.hasOwn(this.values, key)) return {now: true, value: this.values[key]};
    (this.listaners[key] ??= []).push(later);
    return {now: false};
  },
  async setValue(key, value) {
    this.values[key] = value;
    if (this.listaners[key]) {
      for (let later of this.listaners[key]) await later(value);
      delete this.listaners[key];
    }
  }
};

Data.getValue("username", username => {
  for (let link of document.querySelectorAll("#filetable td:nth-child(2) a")) link.href = link.href.replace("ObserverOfVoid", username);
});

Data.getValue("exportTime", et => {
  EL.exportTime.textContent = et;
  EL.exportTime.parentNode.hidden = false;
});

//Tabs

const Tabs = {
  home: {
    tab: EL.tab.home,
    nav: EL.tabs.select.children[0]
  }
}, tabOrder = ["chat", "subreddits", "misc"];

let activeTab = "home";

async function addTab(name) {
  if (Tabs[name]) return Tabs[name];
  
  let cssLink = document.createElement("link");
  cssLink.rel = "stylesheet";
  cssLink.type = "text/css";
  cssLink.href = `tabs/${name}.css`;
  document.head.append(cssLink);
  
  let tab = document.createElement("div");
  tab.id = `tab.${name}`;
  tab.innerHTML = await fetch(`tabs/${name}.fhtml`).then(res => res.text());
  addEL(tab);
  EL.tab[name] = tab;
  EL.tabs.content.append(tab);
  
  let nav = document.createElement("button");
  nav.textContent = name[0].toUpperCase() + name.slice(1);
  nav.dataset.tab = name;
  EL.tabs.select.append(nav);
  
  let Tab = Tabs[name] = {
    tab,
    nav,
    els: addEL(tab, null)[name]
  };
  
  EL.tabs.select.append(...tabOrder.map(t => Tabs[t]?.nav).filter(t => t));
  
  Tab.module = await import(`./tabs/${name}.js`);
  
  return Tab;
}

function setTab(name) {
  Tabs[activeTab].nav.classList.remove("active");
  EL.tab[activeTab].classList.remove("active");
  activeTab = name;
  Tabs[activeTab].nav.classList.add("active");
  EL.tab[activeTab].classList.add("active");
}

EL.tabs.select.onclick = e => {
  if (e.target.dataset.tab) setTab(e.target.dataset.tab);
};

export {Tabs, Data};