const EL =  Object.create(null);

function addEL(root, target = EL) {
  target ??= Object.create(null);
  for (let el of root.querySelectorAll("[id]")) {
    let El = target, path = el.id.split("."), pl = path.length - 1;
    for (let i = 0; i < pl; i++) El = (El[path[i]] ||= Object.create(null));
    El[path[pl]] = el;
  }
  return target;
}

addEL(document);

const CSV = {
  rowPattern: /(?<=^|\r\n|(?<!\r)\n)([^"\n]|"([^"]|"")*")+(?=$|\r\n|(?<!\r)\n)/gs,
  valuePattern: /(?<=^|,)([^",]|"([^"]|"")*")*(?=,|$)/gs,
  stringPattern: /"(([^"]|"")*)"/gs,
  conversions: {
    identity: str => str,
    int: str => str ? Number.parseInt(str) : null,
    float: str => str ? Number.parseFloat(str) : null
  },
  parseRow(row, convertors = []) {
    let values = [];
    convertors = convertors.values();
    for (let value of row.matchAll(this.valuePattern)) values.push(
      (convertors.next().value || this.conversions.identity)(value[0].replaceAll(
        this.stringPattern,
        (str, content) => content.replaceAll('""', '"')
      ))
    );
    return values;
  },
  parse(csv, convertors = Object.create(null)) {
    let
      rows = csv.matchAll(this.rowPattern),
      header = this.parseRow(rows.next().value[0]),
      data = [],
      rowConvertors = header.map(field => convertors[field]);
    for (let row = rows.next(); !row.done; row = rows.next()) {
      let
        values = this.parseRow(row.value[0], rowConvertors),
        obj = Object.create(null);
      for (let i = 0; i < header.length; i++) obj[header[i]] = values[i];
      data.push(obj);
    }
    return data;
  }
};

export {EL, addEL, CSV};