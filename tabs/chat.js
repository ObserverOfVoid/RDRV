import {Tabs, Data} from "../index.js";

const Tab = Tabs.chat, els = Tab.els;

const channels = Object.create(null), orders = {
  oldest: (c1, c2) => c1.messages[0].created_at.time - c2.messages[0].created_at.time,
  mostRecent: (c1, c2) => c2.messages.at(-1).created_at.time - c1.messages.at(-1).created_at.time
};

let currentChannel = null;

function loadData(data) {
  for (let message of data) {
    let channel = channels[message.channel_url] ??= {
      messages: [],
      members: new Set()
    };
    delete message.channel_url;
    channel.messages.push(message);
    channel.members.add(message.username);
  }
  
  let users = Object.create(null);
  for (let [id, channel] of Object.entries(channels)) {
    channel.messages.sort((m1, m2) => m1.created_at.time - m2.created_at.time);
    
    channel.title = channel.messages.at(-1).channel_name;
    if (channel.title === "N/A") channel.title = id;
    
    channel.button = document.createElement("a");
    channel.button.onclick = e => {
      e.preventDefault();
      setChannel(id);
    };
    channel.button.href = "";
    
    channel.header = document.createElement("div");
    els.members.append(channel.header);
    
    channel.body = document.createElement("div");
    let username = null, block;
    for (let message of channel.messages) {
      if (message.username !== username) {
        username = message.username;
        let bblock = document.createElement("div");
        bblock.dataset.username = username;
        block = document.createElement("div");
        block.classList.add("block");
        bblock.append(block);
        let name = document.createElement("div");
        name.classList.add("username");
        name.textContent = username;
        block.append(name);
        channel.body.append(bblock);
      }
      let time = document.createElement("time");
      time.textContent = message.created_at.str;
      block.append(time);
      let body = document.createElement("div");
      body.classList.add("body");
      if (message.message.startsWith("https://i.redd.it/")) {
        let img = document.createElement("img");
        img.src = message.message;
        body.append(img);
      } else body.textContent = message.message;
      block.append(body);
    }
    els.messages.append(channel.body);
    
    channel.members = [...channel.members].sort();
    for (let member of channel.members) {
      users[member] ??= 0;
      users[member]++;
    }
  }
  
  for (let order in orders) orders[order] = Object.values(channels).sort(orders[order]).map(c => c.button);
  let ord = els.order;
  (ord.onchange = () => setOrder(ord.value))();
  orders[ord.value][0].click();
  
  let pers = els.perspective;
  for (let user of Object.entries(users).sort(
    (u1, u2) => u1[1] > u2[1] ? -1 : u1[1] < u2[1] ? 1 : u1[0] < u2[0] ? -1 : 1
  )) {
    let option = document.createElement("option");
    option.value = user[0];
    option.innerHTML = user[0];
    pers.append(option);
  }
  let username = Data.getValue("username", uname => {
    pers.value = uname;
    setPerspective(uname);
  });
  username = (username.now ? username : pers).value;
  pers.value = username;
  setPerspective(username);
  pers.onchange = () => setPerspective(pers.value);
}

const activeElements = ["button", "header", "body"];

function setChannel(newChannel) {
  if (currentChannel) for (let ae of activeElements) channels[currentChannel][ae].classList.remove("active");
  currentChannel = newChannel;
  for (let ae of activeElements) channels[newChannel][ae].classList.add("active");
}

function setPerspective(perspective) {
  for (let channel of Object.values(channels)) {
    let members = channel.members.filter(name => name !== perspective);
    channel.button.textContent = members.join(", ") || channel.title;
    if (members.length > 0) {
      channel.header.replaceChildren(...members.flatMap((member, sep) => {
        let link = document.createElement("a");
        link.href = `https://old.reddit.com/user/${member}/`;
        link.textContent = member;
        return sep ? [", ", link] : link;
      }));
    } else channel.header.textContent = channel.title;
    for (let block of channel.body.children) block.classList.toggle("perspective", block.dataset.username === perspective);
  }
}

function setOrder(order) {
  els.list.append(...orders[order]);
}

export {loadData};