import {Tabs, Data} from "../index.js";

const Tab = Tabs.misc, els = Tab.els;

const statistics = Object.create(null);

function loadStatistics(stats) {
  for (let stat of stats) statistics[stat.statistic] = stat.value;
  
  let username = statistics["account name"];
  Data.setValue("username", username);
  let profile = document.createElement("a");
  profile.textContent = username;
  profile.href = `https://old.reddit.com/user/${username}/`;
  els.username.append(profile);
  if (statistics.is_deleted === "True") {
    profile.classList.add("deleted");
    els.username.append(" [deleted]");
  }
  els.username.parentNode.hidden = false;
  
  els.registered.textContent = statistics["registration date"];
  els.registered.parentNode.parentNode.hidden = false;
  
  let email = statistics["email address"];
  let mailto = document.createElement("a");
  mailto.textContent = email;
  mailto.href = `mailto:${email}`;
  els.email.append(mailto, ` (${statistics["email verified"] === "True" ? "" : "not "}verified)`);
  els.email.parentNode.hidden = false;
  
  Data.setValue("exportTime", statistics["export time"]);
  
  els.account.hidden = false;
}

function loadGender(gender) {
  gender = gender[0].account_gender;
  
  els.gender.textContent = gender;
  els.gender.parentNode.hidden = false;
  
  els.account.hidden = false;
}

export {loadStatistics, loadGender};