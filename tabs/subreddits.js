import {Tabs, Data} from "../index.js";

const Tab = Tabs.subreddits, els = Tab.els;

const order = ["member", "approved", "mod"], labels = {
  member: "Member",
  approved: "Approved user",
  mod: "Moderator"
};

const subreddits = Object.create(null), heads = Object.create(null);

function createRow(name) {
  let o = {
    row: document.createElement("tr"),
    cols: Object.create(null)
  };
  let td = document.createElement("td");
  let link = document.createElement("a");
  link.textContent = name;
  link.href = `https://old.reddit.com/r/${name}/`;
  td.append(link);
  o.row.append(td);
  return o;
}

function add(type, data) {
  let head = heads[type] = document.createElement("th");
  head.textContent = labels[type];
  for (let item of data) {
    let subreddit = subreddits[item.subreddit.toLowerCase()] ??= createRow(item.subreddit);
    let col = subreddit.cols[type] = document.createElement("td");
    col.classList.add("o");
  }
  for (let c of order) {
    if (!heads[c]) continue;
    els.head.append(heads[c]);
    for (let subreddit of Object.values(subreddits)) {
      if (!subreddit.cols[c]) {
        let col = subreddit.cols[c] = document.createElement("td");
        col.classList.add("x");
      }
      subreddit.row.append(subreddit.cols[c]);
    }
  }
  for (let [name, subreddit] of Object.entries(subreddits).sort()) els.body.append(subreddit.row);
}

export {add};